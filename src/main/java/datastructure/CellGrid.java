package datastructure;

import java.util.Arrays;

import javax.sql.RowSet;

import cellular.CellState;

public class CellGrid implements IGrid {

    int rows;
    int cols;
    CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.cols = columns;
        this.grid = new CellState[rows][columns];
        for (CellState[] row: grid) {
            Arrays.fill(row, initialState);
        }
    }

    @Override
    public int numRows() {
        int numRows = this.rows;
        return numRows;
    }

    @Override
    public int numColumns() {
        int numCols = this.cols;
        return numCols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        this.grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        return this.grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid gridCopy = new CellGrid(this.numRows(), this.numColumns(), CellState.ALIVE);
        for(int i=0; i<gridCopy.numRows(); i++)
            gridCopy.grid[i] = Arrays.copyOf(this.grid[i], this.grid.length);
        return gridCopy;
    }

}
